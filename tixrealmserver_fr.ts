<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fr_FR">
<context>
    <name>ConnDetail</name>
    <message>
        <location filename="connthread.cpp" line="1253"/>
        <source>Error coping the file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="connthread.cpp" line="1442"/>
        <location filename="connthread.cpp" line="1491"/>
        <location filename="connthread.cpp" line="1577"/>
        <location filename="connthread.cpp" line="1734"/>
        <location filename="connthread.cpp" line="2111"/>
        <location filename="connthread.cpp" line="2547"/>
        <location filename="connthread.cpp" line="3116"/>
        <location filename="connthread.cpp" line="3223"/>
        <source>Wrong xml request: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="connthread.cpp" line="1545"/>
        <location filename="connthread.cpp" line="1631"/>
        <source>Error writing to server file system</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="connthread.cpp" line="1757"/>
        <source>Les dimensions du fichier ne sont pas valides</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="connthread.cpp" line="1792"/>
        <source>Format de fichier invalide: Mauvaise definition de zones.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="connthread.cpp" line="1827"/>
        <location filename="connthread.cpp" line="1834"/>
        <source>Format de fichier invalide: Mauvaise definition de la structure de numérotation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="connthread.cpp" line="1840"/>
        <source>Mauvaise definition de la structure de numérotation. La valeur level doit ?tre unique</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="connthread.cpp" line="1876"/>
        <source>Format invalide pour l&apos;element</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="connthread.cpp" line="1995"/>
        <source>Invalid file format.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="connthread.cpp" line="2250"/>
        <location filename="connthread.cpp" line="2334"/>
        <source>Inconsistent data found. Please contact your technical service.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="connthread.cpp" line="2413"/>
        <location filename="connthread.cpp" line="2425"/>
        <source>Invalid table size.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="connthread.cpp" line="2434"/>
        <source>Wrong xml request: invalid parameter.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="connthread.cpp" line="2437"/>
        <source>Wrong xml request: invalid command.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="connthread.cpp" line="2441"/>
        <source>Wrong xml request: command not found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="connthread.cpp" line="2842"/>
        <source>There are not enough products.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="connthread.cpp" line="3138"/>
        <source>This is not a valid login command.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="connthread.cpp" line="3166"/>
        <source>Error opening a database connection.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="connthread.cpp" line="3177"/>
        <source>Cannot open a session for this credentials.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="connthread.cpp" line="3225"/>
        <source>Wrong xml request.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="connthread.cpp" line="3279"/>
        <location filename="connthread.cpp" line="3287"/>
        <source>Unknown request type.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="connthread.cpp" line="3309"/>
        <source>There is not a valid server session</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="connthread.cpp" line="3354"/>
        <source>Error opening a database connection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="connthread.cpp" line="3369"/>
        <source>Error trying to register session</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="connthread.cpp" line="3376"/>
        <source>Unknown request type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="connthread.cpp" line="3454"/>
        <source>Wrong xml request: parameters not found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="connthread.cpp" line="3460"/>
        <source>Wrong xml request: unknown command.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="connthread.cpp" line="3464"/>
        <source>Wrong xml request: command item not found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="connthread.cpp" line="3491"/>
        <source>Unknown HTTP header.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="connthread.cpp" line="3509"/>
        <source>Invalid request method.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="connthread.cpp" line="3519"/>
        <source>Unsupported HTTP protocol.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="connthread.cpp" line="3561"/>
        <source>Unsupported content enconding. Accepted values: deflate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="connthread.cpp" line="3595"/>
        <source>Invalid request: can not find Content-Length.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FnacServiceInterface</name>
    <message>
        <location filename="fnacservicetools.cpp" line="101"/>
        <location filename="fnacservicetools.cpp" line="153"/>
        <source>Seance not found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fnacservicetools.cpp" line="122"/>
        <location filename="fnacservicetools.cpp" line="198"/>
        <source>Blocs not found for tarif: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fnacservicetools.cpp" line="162"/>
        <source>No Contingent information found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fnacservicetools.cpp" line="168"/>
        <source>Only one type of Contingent can be sent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fnacservicetools.cpp" line="207"/>
        <source>Quantity nbPlaces must not be negative.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fnacservicetools.cpp" line="432"/>
        <location filename="fnacservicetools.cpp" line="477"/>
        <source>Invalid configuration file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fnacservicetools.cpp" line="437"/>
        <location filename="fnacservicetools.cpp" line="482"/>
        <source>Configuration file not found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fnacservicetools.cpp" line="458"/>
        <location filename="fnacservicetools.cpp" line="516"/>
        <source>Cannot find Fnac codification for variable &quot;%1&quot; and value &quot;%2&quot;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fnacservicetools.cpp" line="641"/>
        <source>File %1 not found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fnacservicetools.cpp" line="648"/>
        <source>Invalid file: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fnacservicetools.cpp" line="671"/>
        <source>Error inserting seanse db record.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fnacservicetools.cpp" line="729"/>
        <source>Error inserting place db record.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fnacservicetools.cpp" line="799"/>
        <location filename="fnacservicetools.cpp" line="1858"/>
        <source>Bad soap response: bloc&apos;s code not found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fnacservicetools.cpp" line="810"/>
        <location filename="fnacservicetools.cpp" line="1869"/>
        <source>Bad soap response: bloc&apos;s type not found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fnacservicetools.cpp" line="824"/>
        <source>Bad soap response: bloc&apos;s jaugeBloc not found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fnacservicetools.cpp" line="835"/>
        <source>Bad soap response: bloc&apos;s nbPlacesOption not found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fnacservicetools.cpp" line="848"/>
        <source>Bad soap response: bloc&apos;s nbPlacesPayees not found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fnacservicetools.cpp" line="859"/>
        <source>Bad soap response: bloc&apos;s nbPlacesVente not found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fnacservicetools.cpp" line="870"/>
        <source>Bad soap response: bloc&apos;s nbPlacesCont not found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fnacservicetools.cpp" line="881"/>
        <source>Bad soap response: bloc&apos;s nbPlacesProd not found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fnacservicetools.cpp" line="892"/>
        <source>Bad soap response: bloc&apos;s nbPlacesVenteAutres not found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fnacservicetools.cpp" line="903"/>
        <source>Bad soap response: bloc&apos;s nbPlacesContAutres not found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fnacservicetools.cpp" line="914"/>
        <source>Bad soap response: bloc&apos;s nbPlacesProdAutres not found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fnacservicetools.cpp" line="927"/>
        <source>Bad soap response: bloc&apos;s nbPlacesTotal not found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fnacservicetools.cpp" line="938"/>
        <source>Bad soap response: places not found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fnacservicetools.cpp" line="953"/>
        <source>Bloc not found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fnacservicetools.cpp" line="1144"/>
        <source>Error trying to update Place DB record.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fnacservicetools.cpp" line="1210"/>
        <source>Bad soap response: requestId&apos;s code not found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fnacservicetools.cpp" line="1693"/>
        <source>Bad soap response: titreManifestation1 not found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fnacservicetools.cpp" line="1719"/>
        <location filename="fnacservicetools.cpp" line="1748"/>
        <source>Bad soap response: Seances&apos;s list not found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fnacservicetools.cpp" line="1763"/>
        <source>Partial deallocation.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fnacservicetools.cpp" line="1807"/>
        <source>Bad soap response: bloc&apos;s list not found.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="simpleclicservice.cpp" line="10"/>
        <source>Serveur SimpleCLIC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="simpleclicservice.cpp" line="14"/>
        <source>Serveur http dédié ? la billetterie électronique</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="simpleclicservice.cpp" line="36"/>
        <source>Error in server configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="simpleclicservice.cpp" line="48"/>
        <source>Failed to bind to port %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="simpleclicservice.cpp" line="59"/>
        <source>Failed to open a DB conexion</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="simpleclicservice.cpp" line="70"/>
        <source>Error loading SimpleCLIC xml schemas</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
