<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="en_US">
<context>
    <name>ConnDetail</name>
    <message>
        <location filename="clientconnthread.cpp" line="240"/>
        <source>Not connected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="clientconnthread.cpp" line="240"/>
        <source>Nothing to send</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="clientconnthread.cpp" line="264"/>
        <source>Unknown HTTP response header</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="clientconnthread.cpp" line="271"/>
        <source>Bad response: Unsupported protocol</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="clientconnthread.cpp" line="309"/>
        <source>Unknown encoding</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="clientconnthread.cpp" line="324"/>
        <source>Bad response: unknown Content-Length</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="clientconnthread.cpp" line="510"/>
        <source>Invalid file format.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="clientconnthread.cpp" line="761"/>
        <location filename="clientconnthread.cpp" line="774"/>
        <location filename="clientconnthread.cpp" line="783"/>
        <source>Invalid table size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="clientconnthread.cpp" line="795"/>
        <source>Wrong xml request: unexpected response</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="clientconnthread.cpp" line="799"/>
        <source>Wrong xml request: invalid response</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="clientconnthread.cpp" line="1027"/>
        <source>Wrong xml response: %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.cpp" line="209"/>
        <source>Aquí no hay nada todavía</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PrintJob</name>
    <message>
        <location filename="printmanager.cpp" line="551"/>
        <source>Page %1 of %2 has been printed</source>
        <oldsource>Page %1 de %2 est imprimée</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="printmanager.cpp" line="597"/>
        <source>Abort printing</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PrintManager</name>
    <message>
        <location filename="printmanager.cpp" line="242"/>
        <source>Printing has finished</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="printmanager.cpp" line="244"/>
        <source>Printing has started</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="printmanager.cpp" line="246"/>
        <source>Printing has been paused</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="printmanager.cpp" line="248"/>
        <source>Printing has been resumed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="printmanager.cpp" line="250"/>
        <source>Printing has been terminated</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="printmanager.cpp" line="252"/>
        <source>Are you sure you wish to abort the print job?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="printmanager.cpp" line="254"/>
        <source>Image conversion error (data buffer empty after buffer saved)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="printmanager.cpp" line="256"/>
        <source>Invalid image error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="printmanager.cpp" line="258"/>
        <source>Error generating the report pages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="printmanager.cpp" line="260"/>
        <source>The print job failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="printmanager.h" line="51"/>
        <source>Status %2: %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PrintProgressWidget</name>
    <message>
        <location filename="printmanager.cpp" line="866"/>
        <source>Printing tickets (%v de %m)...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="printmanager.cpp" line="868"/>
        <source>Printing tickets...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="main.cpp" line="96"/>
        <source>This system does not support OpenSSL.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="scstockwidget.cpp" line="674"/>
        <source> à </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="sccartwidget.cpp" line="451"/>
        <source>Empty shopping cart!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="sccartwidget.cpp" line="457"/>
        <source>Order is already been paid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="sccartwidget.cpp" line="464"/>
        <source>There is no payment!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="sccartwidget.cpp" line="683"/>
        <source>This shopping cart can&apos;t be removed.</source>
        <oldsource>This shopping cart can&apos;t be removed));</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="sccartwidget.cpp" line="689"/>
        <source>This shopping cart is not empty, all items will be removed. Are you sure?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="sccartwidget.cpp" line="1225"/>
        <source>A shopping cart already exist for this client.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="sccartwidget.cpp" line="1238"/>
        <source>Can&apos;t rename this shopping cart.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="sctransportwidget.cpp" line="317"/>
        <source>Select Voyage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="sctransportwidget.cpp" line="324"/>
        <source>Select Dates</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="sctransportwidget.cpp" line="331"/>
        <source>Select Column Price</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="sctransportwidget.cpp" line="369"/>
        <source>Date Init is greater than Date End</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ReservoirDialog</name>
    <message utf8="true">
        <location filename="../ClientEditorSharedSources/reservoirdialog.ui" line="14"/>
        <source>Details du réservoir[*]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ClientEditorSharedSources/reservoirdialog.ui" line="32"/>
        <source>Nom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ClientEditorSharedSources/reservoirdialog.ui" line="42"/>
        <source>Couleur</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../ClientEditorSharedSources/reservoirdialog.ui" line="89"/>
        <source>Zone non placée</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ClientEditorSharedSources/reservoirdialog.ui" line="101"/>
        <source>Nombre de places</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../ClientEditorSharedSources/reservoirdialog.ui" line="117"/>
        <source>Numéro unique</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ScAcceuilWidget</name>
    <message>
        <location filename="scacceuilwidget.cpp" line="226"/>
        <source>Especes theoriques</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="scacceuilwidget.cpp" line="236"/>
        <source>Fond de caisse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="scacceuilwidget.cpp" line="247"/>
        <source>A remettre</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="scacceuilwidget.cpp" line="257"/>
        <source>Especes reelles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="scacceuilwidget.cpp" line="269"/>
        <source>Total theorique</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="scacceuilwidget.cpp" line="279"/>
        <source>Total reel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ScCartWidget</name>
    <message>
        <location filename="sccartwidget.cpp" line="89"/>
        <location filename="sccartwidget.cpp" line="662"/>
        <source>Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="sccartwidget.cpp" line="89"/>
        <location filename="sccartwidget.cpp" line="662"/>
        <source>Devise</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="sccartwidget.cpp" line="89"/>
        <location filename="sccartwidget.cpp" line="662"/>
        <source>Montant</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="sccartwidget.cpp" line="89"/>
        <location filename="sccartwidget.cpp" line="662"/>
        <source>Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="sccartwidget.cpp" line="89"/>
        <location filename="sccartwidget.cpp" line="662"/>
        <source>Payeur</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="sccartwidget.cpp" line="683"/>
        <location filename="sccartwidget.cpp" line="689"/>
        <location filename="sccartwidget.cpp" line="1225"/>
        <location filename="sccartwidget.cpp" line="1238"/>
        <source>Shopping cart</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <location filename="sccartwidget.cpp" line="806"/>
        <source>There are %n item(s) &quot;%1&quot; in the cart</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location filename="sccartwidget.cpp" line="807"/>
        <source>Howmany do you want to remove?</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <location filename="sccartwidget.cpp" line="812"/>
        <source>You are trying to remove %n item(s) from the cart:

%2

Are you sure?</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="sccartwidget.cpp" line="1032"/>
        <source>Error trying to remove the following item(s)

</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
</context>
<context>
    <name>ScConfigWidget</name>
    <message>
        <location filename="scconfigwidget.cpp" line="10"/>
        <source>Configuration</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ScCrmWidget</name>
    <message>
        <location filename="sccrmwidget.cpp" line="9"/>
        <source>CRM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="sccrmwidget.cpp" line="38"/>
        <source>Wait, please. Response is not ready</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ScGrillesAdminPage</name>
    <message>
        <location filename="scgrillesadminpage.cpp" line="364"/>
        <source>Price table administration</source>
        <oldsource>Administration des Grilles de tarifs</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="scgrillesadminpage.cpp" line="666"/>
        <location filename="scgrillesadminpage.cpp" line="758"/>
        <source>User category</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="scgrillesadminpage.cpp" line="794"/>
        <source>Invalid name.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="scgrillesadminpage.cpp" line="800"/>
        <location filename="scgrillesadminpage.cpp" line="808"/>
        <source>Duplicated name.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="scgrillesadminpage.cpp" line="827"/>
        <source>Product category</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="scgrillesadminpage.cpp" line="1329"/>
        <source>Cli. Cat. 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="scgrillesadminpage.cpp" line="1330"/>
        <source>Prod. Cat. 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="scgrillesadminpage.cpp" line="1425"/>
        <source>New price table</source>
        <oldsource>Nouvelle grille</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="scgrillesadminpage.cpp" line="1476"/>
        <source>Invalid reponse.</source>
        <oldsource>Reponse non valide</oldsource>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ScNewSeanceWidget</name>
    <message>
        <location filename="scnewseancewidget.cpp" line="26"/>
        <source>Nouvelle séance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="scnewseancewidget.cpp" line="100"/>
        <source>You should choice a place&apos;s configuration file.</source>
        <oldsource>Il faut choisir d&apos;abord un plan de salle.</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="scnewseancewidget.cpp" line="94"/>
        <source>You should choice a price table.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="scnewseancewidget.cpp" line="108"/>
        <source>Can&apos;t open the place&apos;s configuration file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="scnewseancewidget.cpp" line="264"/>
        <source>Open Ticket Config.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="scnewseancewidget.cpp" line="264"/>
        <source>Xml Files (*.xml)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="scnewseancewidget.cpp" line="272"/>
        <source>Open Place Config.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="scnewseancewidget.cpp" line="272"/>
        <source>Svg Files (*.svg)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ScNewSpectWidget</name>
    <message>
        <location filename="scnewspectwidget.cpp" line="7"/>
        <source>New performance</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ScStockWidget</name>
    <message>
        <location filename="scstockwidget.cpp" line="747"/>
        <source>Unable to attach to shared memory segment: </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ScTicketingStockWidget</name>
    <message>
        <location filename="scticketingstockwidget.cpp" line="14"/>
        <source>Billeterie</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ScTransportStockWidget</name>
    <message>
        <location filename="sctransportstockwidget.cpp" line="14"/>
        <source>Voyages</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ScTransportWidget</name>
    <message>
        <location filename="sctransportwidget.cpp" line="17"/>
        <source>Transport</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ScWidget</name>
    <message>
        <location filename="scwidget.cpp" line="20"/>
        <source>Cannot load the graphical user interface.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="scwidget.cpp" line="80"/>
        <source>Open Image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="scwidget.cpp" line="80"/>
        <source>Image Files (*.png *.jpg *.bmp)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="scwidget.cpp" line="86"/>
        <source>File not found</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SchemaNumDialog</name>
    <message utf8="true">
        <location filename="../ClientEditorSharedSources/schemanumdialog.ui" line="14"/>
        <source>Structure de numérotation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ClientEditorSharedSources/schemanumdialog.ui" line="29"/>
        <source>Nom</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../ClientEditorSharedSources/schemanumdialog.ui" line="39"/>
        <source>Série de numérotation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ClientEditorSharedSources/schemanumdialog.ui" line="47"/>
        <source>1, 2...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ClientEditorSharedSources/schemanumdialog.ui" line="52"/>
        <source>1,3...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ClientEditorSharedSources/schemanumdialog.ui" line="57"/>
        <source>2,4...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ClientEditorSharedSources/schemanumdialog.ui" line="62"/>
        <source>A, B...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ClientEditorSharedSources/schemanumdialog.ui" line="67"/>
        <source>AA, BB...</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../ClientEditorSharedSources/schemanumdialog.ui" line="72"/>
        <source>Nouvelle série</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../ClientEditorSharedSources/schemanumdialog.ui" line="80"/>
        <source>Taille de la série</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ClientEditorSharedSources/schemanumdialog.ui" line="100"/>
        <source>Utiliser les valeur par defaut</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SerieDialog</name>
    <message>
        <location filename="../ClientEditorSharedSources/seriedialog.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ClientEditorSharedSources/seriedialog.ui" line="33"/>
        <location filename="../ClientEditorSharedSources/seriedialog.ui" line="50"/>
        <source>,</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ClientEditorSharedSources/seriedialog.ui" line="76"/>
        <source>,...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SimpleCLICbureauMainClass</name>
    <message>
        <location filename="SimpleCLICbureauMainClass.cpp" line="248"/>
        <source>Invalid response</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Site2dView</name>
    <message>
        <location filename="../ClientEditorSharedSources/site2dview.cpp" line="332"/>
        <location filename="../ClientEditorSharedSources/site2dview.cpp" line="350"/>
        <source>Nombre de places</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ClientEditorSharedSources/site2dview.cpp" line="333"/>
        <source>Zone placée</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ClientEditorSharedSources/site2dview.cpp" line="334"/>
        <source>Numéro unique</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ClientEditorSharedSources/site2dview.cpp" line="335"/>
        <source>Prix</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ClientEditorSharedSources/site2dview.cpp" line="349"/>
        <source>Nom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ClientEditorSharedSources/site2dview.cpp" line="351"/>
        <source>1er rang en sens direct</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ClientEditorSharedSources/site2dview.cpp" line="411"/>
        <source>Le fichier indiqué n&apos;existe pas!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ClientEditorSharedSources/site2dview.cpp" line="790"/>
        <location filename="../ClientEditorSharedSources/site2dview.cpp" line="1291"/>
        <source>Format de fichier invalide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ClientEditorSharedSources/site2dview.cpp" line="798"/>
        <source>Les dimensions du fichier ne sont pas valides</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ClientEditorSharedSources/site2dview.cpp" line="836"/>
        <source>Format de fichier invalide: Mauvaise definition de zones.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ClientEditorSharedSources/site2dview.cpp" line="854"/>
        <location filename="../ClientEditorSharedSources/site2dview.cpp" line="2017"/>
        <source>Non</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ClientEditorSharedSources/site2dview.cpp" line="854"/>
        <location filename="../ClientEditorSharedSources/site2dview.cpp" line="1534"/>
        <location filename="../ClientEditorSharedSources/site2dview.cpp" line="2017"/>
        <source>Oui</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ClientEditorSharedSources/site2dview.cpp" line="891"/>
        <source>Format de fichier invalide: Mauvaise definition de la structure de numérotation.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ClientEditorSharedSources/site2dview.cpp" line="925"/>
        <source>Mauvaise definition de la structure de numérotation. La valeur level doit ?tre unique</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ClientEditorSharedSources/site2dview.cpp" line="965"/>
        <source>Format invalide pour l&apos;element</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ClientEditorSharedSources/site2dview.cpp" line="990"/>
        <source>Bloc invalide pour l&apos;element</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ClientEditorSharedSources/site2dview.cpp" line="1346"/>
        <location filename="../ClientEditorSharedSources/site2dview.cpp" line="1476"/>
        <source>Imposible de creer le fichier resultant</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ClientEditorSharedSources/site2dview.cpp" line="1975"/>
        <location filename="../ClientEditorSharedSources/site2dview.cpp" line="2033"/>
        <source>Il faut d&apos;abord selectionner une zone.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ClientEditorSharedSources/site2dview.cpp" line="2142"/>
        <location filename="../ClientEditorSharedSources/site2dview.cpp" line="2187"/>
        <location filename="../ClientEditorSharedSources/site2dview.cpp" line="2353"/>
        <location filename="../ClientEditorSharedSources/site2dview.cpp" line="2668"/>
        <source>Il faut d&apos;abord sélectionner des places.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ClientEditorSharedSources/site2dview.cpp" line="2149"/>
        <source>Il faut d&apos;abord sélectionner une zone.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ClientEditorSharedSources/site2dview.cpp" line="2247"/>
        <source>Il n&apos;y a pas des element a numéroter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ClientEditorSharedSources/site2dview.cpp" line="2360"/>
        <location filename="../ClientEditorSharedSources/site2dview.cpp" line="2511"/>
        <source>Il faut d&apos;abord sélectionner au moins un element de numérotation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ClientEditorSharedSources/site2dview.cpp" line="2370"/>
        <location filename="../ClientEditorSharedSources/site2dview.cpp" line="2519"/>
        <source>Impossible modifier les element de ce niveau</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ClientEditorSharedSources/site2dview.cpp" line="2858"/>
        <source>Il faut d&apos;abord sélectionner au moins un bloc.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ClientEditorSharedSources/site2dview.cpp" line="3000"/>
        <source>Il faut selectioné au moins un element</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ClientEditorSharedSources/site2dview.cpp" line="3043"/>
        <source>Il y a des param?tres incompatibles dans la sélection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ClientEditorSharedSources/site2dview.cpp" line="3082"/>
        <location filename="../ClientEditorSharedSources/site2dview.cpp" line="3146"/>
        <source>Il faut d&apos;abord sélectionner un element de la structure de numerotation.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ClientEditorSharedSources/site2dview.cpp" line="3089"/>
        <source>Impossible d&apos;inserer un nouvel element dans cette possition de la structure.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ClientEditorSharedSources/site2dview.cpp" line="3153"/>
        <source>Impossible d&apos;effacer cet element de la structure.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ClientEditorSharedSources/site2dview.cpp" line="3299"/>
        <source>Choisisez le fichier de sortir</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ClientEditorSharedSources/site2dview.cpp" line="3299"/>
        <source>Fichiers images (*.png *.bmp *.jpg)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SpinBoxDelegate</name>
    <message>
        <location filename="../ClientEditorSharedSources/site2dview.cpp" line="139"/>
        <source>Il existe déj? un autre bloc avec la m?me priorité.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ZoneDialog</name>
    <message>
        <location filename="../ClientEditorSharedSources/zonedialog.ui" line="14"/>
        <source>Details de la zone[*]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ClientEditorSharedSources/zonedialog.ui" line="32"/>
        <source>Nom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ClientEditorSharedSources/zonedialog.ui" line="42"/>
        <source>Couleur</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../ClientEditorSharedSources/zonedialog.ui" line="89"/>
        <source>Zone non placée</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ClientEditorSharedSources/zonedialog.ui" line="101"/>
        <source>Nombre de places</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../ClientEditorSharedSources/zonedialog.ui" line="117"/>
        <source>Numéro unique</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
